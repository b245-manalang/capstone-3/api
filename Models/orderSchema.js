const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, 'Full Name is required!']
	},
	productName: {
		type: String,
		required: [true, "Product Name is required!"]
	},
	quantity: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model('Order', orderSchema);