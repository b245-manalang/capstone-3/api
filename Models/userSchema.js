const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, 'First Name is required!']
	},
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	confirmPassword: {
		type: String,
		required: [true, 'Confirm Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	myOrders: [
			{
				productId: {
					type: String,
					required: [true, "Product ID is required!"]
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
})


module.exports = mongoose.model('User', userSchema);