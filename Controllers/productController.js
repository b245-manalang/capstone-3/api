const Product = require('../Models/productSchema.js');
const auth = require('../auth.js');

// Create Product (Admin Only)
module.exports.addProduct = (req, res) =>{
	const userAdmin = auth.decode(req.headers.authorization);

	if(userAdmin.isAdmin === true){
			let input = req.body;

			let newProduct = new Product({
				name: input.name,
				description: input.description,
				price: input.price
			});
		
			//saves the created object to our database
			return newProduct.save()
			.then(product =>{
				// console.log(product);
				res.send(true);
			})
			// course creation failed
			.catch(error => {
				// console.log(error);
				res.send(false);
			})
		}else{
			return res.send(false);
		}
	}


// Retrieve all products
module.exports.allProducts = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// console.log(userData);

	if(!userData.isAdmin){
		return res.send(false);
	}else{
	Product.find()
		.then(result => res.send(result))
		.catch(error => res.send(false));
	}
}
// Retrieve all active products
module.exports.allActiveProducts = (req, res) => {

	Product.find({isAvailable: true})
		.then(result => res.send(result))
		.catch(error => res.send(false));
}


// Retrieve single product
module.exports.productDetails = (req, res) => {
	
	const productId = req.params.productId;

	Product.findById(productId)
		.then(result => res.send(result))
		.catch(error => res.send(false));
}


// Update product information (Admin Only)
module.exports.updateProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const productId = req.params.productId;
	const input = req.body

	if(!userData.isAdmin){
		return res.send(false);
	}else{
		Product.findById(productId)
		.then(result =>{
			if (result === null){
				return res.send(false)
		}else{
		let updatedProduct = {
			name: input.name,
			description: input.description,
			price: input.price
		}

		Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
		.then(result => {
			
			return res.send('true')})
		.catch(error => res.send(false));
		}
	})
	}
}


// Archive Product (Admin Only)
module.exports.archiveProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const productId = req.params.productId;
	const input = req.body

	if(!userData.isAdmin){
		return res.send(false);
	}else{
		Product.findById(productId)
		.then(result =>{
			if (result === null){
				return res.send(false)
		}else{
		let archivedProduct = {
			isAvailable: input.isAvailable
		}

		Product.findByIdAndUpdate(productId, archivedProduct, {new: true})
		.then(result => {
			return res.send(true)})
		.catch(error => res.send(false));
		}
	})
	}
}