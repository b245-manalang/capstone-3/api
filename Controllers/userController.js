const mongoose = require('mongoose');
const User = require('../Models/userSchema.js');

const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Controllers

//User Registration
module.exports.userRegistration = async(req, res) => {

		const input = req.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return res.send(false)
			}else{
				let newUser = new User({
					fullName: input.fullName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					confirmPassword: bcrypt.hashSync(input.confirmPassword, 10)
				})

				newUser.save()
				.then(save => {
					return res.send(true)
							})
				.catch(err => {
					return res.send(false)
				})
			}
		})

		.catch(err => {
			return res.send(false)
		})
}

//User Authentication 	
module.exports.userAuthentication = (req, res) => {
		let input = req.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return res.send(false)
			}else{
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return res.send({auth: auth.createAccessToken(result)});
				}else{
					return res.send(false)
				}
			}
		})

		.catch(err => {
			return res.send(false)
		})
}

//Retrieve User Details
module.exports.getProfile = (req, res) => {
		const userData = auth.decode(req.headers.authorization);

		// console.log(userData);
		
		return User.findById(userData._id).then(result => {
			//avoid to expose sensitive information such as password.
			result.password = "***";
			result.confirmPassword = "***";

			return res.send(result);
		})
	}


//Set user as admin (Admin only)
module.exports.updateUser = (req, res) => {
		const userData = auth.decode(req.headers.authorization);
		const userId = req.params.userId;
		const input = req.body

		if(!userData.isAdmin){
			return res.send(false);
		}else{
			User.findById(userId)
			.then(result =>{
				if (result === null){
					return res.send(false)
			}else{
			let updatedUser = {
				isAdmin: true
			}

			User.findByIdAndUpdate(userId, updatedUser, {new: true})
			.then(result => {
				
				return res.send(true)})
			.catch(error => res.send(false));
			}
		})
		}
	}	

module.exports.purchaseItem = async (req, res) => {
	// First we have to get the userId and the course Id

	//decode the token to extract/unpack the payload
	const userData = auth.decode(req.headers.authorization);
	
	//get the courseId by targetting the params in the url
	const productId = request.params.productId;

	//2 things that we need to do in this controller
		//First, to push the courseId in the enrollments property of the user
		//Second, to push the userId in the enroller's property of the course.


	if(userData.isAdmin){
			return res.send(false);
		}else{
		let isUserUpdated = await User.findById(userData._id)
		.then(result => {
			if(result === null){
				return false
			}else{
				result.myOrders.push({productId: productId});

				return result.save()
				.then(save => true)
				.catch(error => false)
			}
		})

		let isProductUpdated = await Product.findById(productId).then(result => {
			// console.log(result);
			if(result === null){
				return false;
			}else{
				result.purchasedBy.push({userId: userData._id});

				return result.save()
				.then(save => true)
				.catch(error => false);
			}
		})

		// console.log(isProductUpdated);
		// console.log(isUserUpdated);

		if(isProductUpdated && isUserUpdated){
			return res.send(true);
		}else{
			return res.send(false);
		}
	}

	};
