const Order = require('../Models/orderSchema.js');
const User = require('../Models/userSchema.js');
const auth = require('../auth.js');
const Product = require('../Models/productSchema.js');

// Non-admin User checkout (Create Order)
module.exports.createOrder = (req, res) =>{
	const userAdmin = auth.decode(req.headers.authorization);

	if(!userAdmin.isAdmin){
			let input = req.body;

			let newOrder = new Order({
				fullName: input.fullName,
				productName: input.productName,
				quantity: input.quantity
			});
		
			return newOrder.save()
			.then(product =>{
				// console.log(product);
				res.send(true);
			})
			.catch(error => {
				console.log(error);
				res.send(false);
			})
		}else{
			return res.send(false);
		}
	}

// Retrieve all orders (Admin only)
module.exports.allOrders = (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	
	if(!userData.isAdmin){
			return res.send('Sorry, you are not allowed to access this page');
	}else{
		Order.find({})
			.then(result => res.send(result))
			.catch(error => res.send(false));
	}
}

// Retrieve authenticated user's orders (Non-admin only)
/*module.exports.orderHistory = (req, res) => {
	let input = req.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result === null){
			return res.send('Email is not yet registered. Register first before logging in!')
		}else{
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

			if(isPasswordCorrect){
				return res.send({myOrders: myOrders.(result)});
			}else{
				return res.send('Password is incorrect!')
			}
		}
	})

	.catch(err => {
		return res.send(err)
	})
}*/